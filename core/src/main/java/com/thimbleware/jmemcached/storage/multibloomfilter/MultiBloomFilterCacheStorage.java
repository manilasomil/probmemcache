package com.thimbleware.jmemcached.storage.multibloomfilter;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.nio.charset.Charset;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import static org.jboss.netty.buffer.ChannelBuffers.*;
import com.thimbleware.jmemcached.Key;
import com.thimbleware.jmemcached.LocalCacheElement;
import com.thimbleware.jmemcached.storage.CacheStorage;
import com.thimbleware.jmemcached.BloomFilter;

import java.nio.charset.Charset;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.DataInputStream;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class MultiBloomFilterCacheStorage implements CacheStorage<Key, LocalCacheElement>{

        private Map<String,BloomFilter<Key>> hMapBloom;
        private Map<String,LocalCacheElement> resultSet;
        String[] catlist;
        static final Charset charset = Charset.forName("UTF-8");
    
        public MultiBloomFilterCacheStorage(int bits,int expElements,int hashNum,String category) {
            this(bits, expElements, hashNum, category, null); 
        }

        public MultiBloomFilterCacheStorage(int bits,int expElements,int hashNum, String category,String file_Name) {
                   
            String[] file_path = null;
            int filelen = -1;
            if (file_Name != null){
                file_path = file_Name.split(",");
                filelen = file_path.length;
            }
            catlist = category.split(",");
            int i = 0;
            hMapBloom = new HashMap<String,BloomFilter<Key>>();
            resultSet = new HashMap<String,LocalCacheElement>();
                   
            for (String cat : catlist)
            {
                 hMapBloom.put(cat, new BloomFilter<Key>(bits,expElements,hashNum));
                 ChannelBuffer cb = ChannelBuffers.buffer(4);
                 cb.writeBytes(cat.getBytes());
                      
                 LocalCacheElement le = new LocalCacheElement();
                 le.setData(cb);
                 resultSet.put(cat, le);
                 if(i <= filelen)
                 {
                      try
                      { 
                           InputStream in = new FileInputStream(file_path[i]);
                           DataInputStream stream = new DataInputStream(in);
                           BloomFilter<Key> b = hMapBloom.get(cat);
                           b.readFields(stream);
                           in.close();
                           i++;
                      }catch(Exception e){
                      }
                 } 
            }     
                  
        }
        @Override
        public LocalCacheElement putIfAbsent(Key arg0, LocalCacheElement arg1) {
             
            for(String cat :catlist)
            {
                  BloomFilter<Key> catbloom  = hMapBloom.get(cat.toString());
                  if(!catbloom.contains(arg0)){
                      put(arg0,arg1);
                  }
            }

            return null;
        }

        @Override
        public boolean remove(Object arg0, Object arg1) {
                return false;
        }

        @Override
        public LocalCacheElement replace(Key arg0, LocalCacheElement arg1) {
   
            for(String cat :catlist)
            {
                  BloomFilter<Key> catbloom  = hMapBloom.get(cat.toString());  
                  if(catbloom.contains(arg0)){
                      put(arg0,arg1);
                  }
            }  
            return null;
        }

        @Override
        public boolean replace(Key arg0, LocalCacheElement arg1,
                        LocalCacheElement arg2) {
    
                return false;
        }

        @Override
        public void clear() {
                for(String cat :catlist)
                {
                  BloomFilter<Key> catbloom  = hMapBloom.get(cat.toString());
                  catbloom.clear();
                }
        }

        @Override
        public boolean containsKey(Object key) {
                Key k = (Key) key;
                for(String cat :catlist)
                {
                    BloomFilter<Key> catbloom  = hMapBloom.get(cat);
                    if(catbloom.contains(k))
                    {
                         return true;
                    } 
                } 
                return false;
        }

        @Override
        public boolean containsValue(Object value) {

                return false;
        }

        @Override
        public Set<java.util.Map.Entry<Key, LocalCacheElement>> entrySet() {
     
                return null;
        }

        @Override
        public LocalCacheElement  get(Object key) {
    
                Key k = (Key) key;
                for(String cat :catlist)
                {
                   BloomFilter<Key> catbloom  = hMapBloom.get(cat);
                    if(catbloom.contains(k))
                    {
                         LocalCacheElement e = resultSet.get(cat);
                         e.setKey(k);
                         return e;

                    }

                }         
                    return null;
        }
        @Override
        public boolean isEmpty() {
     
                return false;
        }

        @Override
        public Set<Key> keySet() {
 
                return null;
        }

        @Override
        public LocalCacheElement put(Key key, LocalCacheElement cat) {
                
                BloomFilter<Key> inputcat = hMapBloom.get(cat.getData().toString(charset));
                inputcat.add(key);
                return cat;
        }

        @Override
        public void putAll(Map<? extends Key, ? extends LocalCacheElement> m) {

        }

        @Override
        public LocalCacheElement remove(Object key) {
      
              return null;
        }

        @Override
        public int size() {
              int size = 0;
              for(String cat :catlist)
              {
                    BloomFilter<Key> catbloom  = hMapBloom.get(cat.toString());
                    size = size + catbloom.count();
              }
 
              return size;
        }

        @Override
        public Collection<LocalCacheElement> values() {
  
              return null;
        }

        @Override
        public long getMemoryCapacity() {

              return 0;
        }

        @Override
        public long getMemoryUsed() {

              return 0;
        }

        @Override
        public int capacity() {

              return 0;
        }

        @Override
        public void close() throws IOException {

        }
}

